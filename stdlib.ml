(* Like any up-to-date engineer, we use the tau circle constant and not that
 * dreadfully outdated notion of pi *)
let tau = 6.2831853071

let to_degrees = function rad -> (rad *. 360.) /. tau
let to_radians = function deg -> (deg *. tau) /. 360.
let rd f x = truncate (f (to_radians (float x)))
let ra f x = truncate (to_degrees (f (float x)))
let ra2 f x y = truncate (to_degrees (f (float x) (float y)))

let call fname args = match (fname, args) with
  | ("write", _)    -> List.iter (fun x -> print_int x; print_newline ()) args;
                    (Some 1)
  | ("read", [])    -> print_string "> "; Some (read_int ())
  | ("random", [a]) -> Some (Random.int a)
  | ("max", [a; b]) -> Some (max a b)
  | ("min", [a; b]) -> Some (min a b)
  | ("mod", [a; b]) -> Some (a mod b)
  | ("xor", [a; b]) -> Some (a lxor b)
  | ("succ", [a])   -> Some (succ a)
  | ("pred", [a])   -> Some (pred a)
  | ("abs", [a])    -> Some (abs a)
  | ("pow", [a;n])  -> Some (truncate ((float a) ** (float n)))
  | ("sqrt", [a])   -> Some (truncate (sqrt (float a)))
  | ("exp", [a])    -> Some (truncate (exp (float a)))
  | ("log", [a])    -> Some (truncate (log (float a)))
  | ("log10", [a])  -> Some (truncate (log10 (float a)))
  | ("cos", [a])    -> Some (rd cos a)
  | ("sin", [a])    -> Some (rd sin a)
  | ("tan", [a])    -> Some (rd tan a)
  | ("acos", [a])   -> Some (ra acos a)
  | ("asin", [a])   -> Some (ra asin a)
  | ("atan", [a])   -> Some (ra atan a)
  | ("cosh", [a])   -> Some (rd cosh a)
  | ("sinh", [a])   -> Some (rd sinh a)
  | ("tanh", [a])   -> Some (rd tanh a)
  | ("atan2",[a;b]) -> Some (ra2 atan2 a b)
  | _ -> None
