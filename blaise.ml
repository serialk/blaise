let get_ast file =
  begin
    let cin = open_in file in
      Parser.prg Lexer.token (Lexing.from_channel cin)
  end

let pretty_print file =
  let ast = get_ast file in
  Prettyprint.f ast

let eval file =
  let ast = get_ast file in
  Eval.eval ast

let specs =
  [
    ("-eval", Arg.String(eval), "interprets the given script");
  ]

let usage =
  Printf.sprintf "Usage: %s -help | -eval script | script" Sys.argv.(0)

let _ =
  Random.self_init ();
  Arg.parse specs pretty_print usage; ()
