NAME := blaise
DEPS := AUTHORS ast.mli blaise.ml eval.ml lexer.ml Makefile parser.ml\
	prettyprint.ml stdlib.ml
FILES := parser/parser.mly parser/lexer.mll

LOGIN := pietri_a
RENDU := ${LOGIN}-mpoc2012
TARBALL := ${RENDU}.tar.bz2
GROUPE := b2
CURL := curl --silent --output /dev/null -u ${LOGIN}\
	-F "userfile=@${TARBALL};filename=${TARBALL}"\
	-F "GROUPE=${GROUPE}" -F "MAX_FILE_SIZE=2097152" -F "UPLOADED=DONE"
URL := http://rendu.infoprepa.epita.fr/EPITA2016/index.php?TP=20130107

all: ${DEPS}
	ocamlbuild -no-hygiene ${NAME}.native
	cp ${NAME}.native ${NAME}
	ocamlbuild -clean

blaise: all

rendu: ${DEPS} ${FILES} 
	@echo -n "Making tarball ${TARBALL}… "
	@mkdir ${RENDU}
	@for f in ${DEPS} ${FILES}; do cp $$f ${RENDU}; done
	@tar -cjf ${TARBALL} ${RENDU}
	@rm -rf ${RENDU}
	@echo "done !"

rendu_upload: rendu
	@echo "Sending project to rendu.infoprepa.epita.fr…"
	@${CURL} ${URL}
	@echo "Success !"

clean:
	ocamlbuild -clean
	rm -rf ${RENDU} ${TARBALL} ${NAME}
